FROM python:3.9.10-alpine3.14

RUN pip install pipenv
RUN mkdir /app
COPY Pipfile* /app/
COPY cli.py /app/
COPY robot/ /app/robot/
COPY setting.ini /app/

WORKDIR /app/
RUN pipenv install 
CMD [ "pipenv", "run" , "python", "cli.py" ]
