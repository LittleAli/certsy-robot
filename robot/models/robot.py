from re import X
from robot.models.directions import Direction
from robot.models.table import Table

from singleton_decorator import singleton


@singleton
class Robot:

    def __init__(self):
        self._x = 0
        self._y = 0
        self._face = Direction.NORTH
        self._initialized = False
        self._visited_cells = []

    def move(self):
        self.__initialization_check()
        (new_x, new_y) = self.__find_nex_location()
        if not Table().includes(new_x, new_y) or Table().occupied_with_obstacles(new_x, new_y):
            raise IllegalMovementException()
        else:
            self._x = new_x
            self._y = new_y

    def turn_left(self):
        self.__initialization_check()
        
        if self._face == Direction.NORTH:
            self._face = Direction.WEST
            return
        if self._face == Direction.WEST:
            self._face = Direction.SOUTH
            return
        if self._face == Direction.SOUTH:
            self._face = Direction.EAST
            return
        if self._face == Direction.EAST:
            self._face = Direction.NORTH
            return

    def turn_right(self):
        self.__initialization_check()

        if self._face == Direction.NORTH:
            self._face = Direction.EAST
            return
        if self._face == Direction.EAST:
            self._face = Direction.SOUTH
            return
        if self._face == Direction.SOUTH:
            self._face = Direction.WEST
            return
        if self._face == Direction.WEST:
            self._face = Direction.NORTH
            return
    
    def report(self):
        self.__initialization_check()
        return f'{self._x}, {self._y}, {self._face.value}'

    def place(self, x, y, direction):
        if not Table().includes(x,y) or Table().occupied_with_obstacles(x,y):
            raise IllegalMovementException()
        self._x = x
        self._y = y
        self._face = direction
        self._initialized = True

    def find_path(self, destination_x, destination_y):
        from_x = self.get_x();
        from_y = self.get_y();
                
        adjacent_cells = Table().get_adjacent_cells(from_x, from_y)
        cell_stack = adjacent_cells 

        if cell_stack is None:
            raise NoPathException()
        path = []
        self._visited_cells = [(self.get_x(), self.get_y())]

        return self.recursively_look_for_path(path, cell_stack, destination_x, destination_y)
    
    def recursively_look_for_path(self, path, cell_stack, destination_x, destination_y):
        cell = cell_stack.pop(0)
        while cell in self._visited_cells:
            cell = cell_stack.pop(0)
        if cell is None:
            return NoPathException()
        
        self._visited_cells.append(cell)
        path.append(cell)
        
        (x,y) = cell
        if x == destination_x and y == destination_y:
            return path 
        else:
            for cell in Table().get_adjacent_cells(x,y):
                if not cell in self._visited_cells:
                    cell_stack.append(cell)
            result = [(x,y)] + self.recursively_look_for_path(path, cell_stack, destination_x, destination_y)
            return result
            



            

    def get_x(self):
        return self._x
    
    def get_y(self):
        return self._y
    
    def get_face(self):
        return self._face
    
    def __find_nex_location(self):
        if self._face == Direction.NORTH:
            return [self._x, self._y+1]
        if self._face == Direction.SOUTH:
            return [self._x, self._y-1]
        if self._face == Direction.EAST:
            return [self._x+1, self._y]
        if self._face == Direction.WEST:
            return [self._x-1, self._y]

    def __initialization_check(self):
        if not self._initialized:
            raise NotInitializedException()

class IllegalMovementException(Exception):
    pass

class NotInitializedException(Exception):
    pass

class NoPathException(Exception):
    pass