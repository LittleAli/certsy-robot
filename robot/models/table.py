from singleton_decorator import singleton

@singleton
class Table:

    def __init__(self):
        self.width = 0
        self.height = 0
        self.obstacles= []
    
    def initialize(self, width, height, obstacles = []):
        if width < 0 or height < 0:
            raise NegativeDimentionException()
        # TODO check if all obstacles hava x and y 
        for obstacle in obstacles:
            x,y = obstacle
            if x < 0 or x >= width: 
                raise OutOfTableIndexException()
            if y < 0 or x >= height:
                raise OutOfTableIndexException()
        self.width = width
        self.height = height
        self.obstacles = obstacles 
    
    def includes(self, x , y):
        if (0 <= x) and (x < self.width) and (0 <= y) and (y < self.height):
            return True
        return False

    def occupied_with_obstacles(self, x, y):
        for (obstacle_x, obstacle_y) in self.obstacles:
            if obstacle_x == x and obstacle_y == y:
                return True
        return False

    def get_adjacent_cells(self, x, y):
        result = []
        if self.check_if_cell_is_movable(x,y-1):
            result.append((x,y-1))
        if self.check_if_cell_is_movable(x-1,y):
            result.append((x-1,y))
        if self.check_if_cell_is_movable(x,y+1):
            result.append((x,y+1))
        if self.check_if_cell_is_movable(x+1,y):
            result.append((x+1,y)) 
        return result
    
    def check_if_cell_is_movable(self, x, y):
        if self.occupied_with_obstacles(x,y) or not self.includes(x,y):
            return False
        return True

class NegativeDimentionException(Exception):
    pass

class OutOfTableIndexException(Exception):
    pass