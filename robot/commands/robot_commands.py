from robot.commands.command import Command
from robot.models.directions import Direction

import re

from robot.models.robot import Robot

class RightCommand(Command):
    def help(self):
        return 'RIGHT -> Force robot to turn right.'

    def __init__(self):
        self._pattern = re.compile(r'^\s*RIGHT\s*$')

    def _execute(self, command):
        Robot().turn_right()

    def isAcceptable(self, command):
        return self._pattern.match(command)


class ReportCommand(Command):
    def help(self):
        return 'REPORT -> Report the status of robot in X,Y,Direction e.g. 2 ,3 ,SOUTH.'

    def __init__(self):
        self._pattern = re.compile(r'^\s*REPORT\s*$')

    def _execute(self, command):
        return Robot().report()

    def isAcceptable(self, command):
        return self._pattern.match(command)


class PlaceCommand(Command):
    def help(self):
        return 'PLACE X,Y,Z -> Replace robot in X and Y location facing Z direction.\n'\
            f'   Directions are {Direction.EAST}, {Direction.WEST}, {Direction.NORTH}, {Direction.SOUTH}'

    def __init__(self):
        integer_pattern = r'\d+'
        direction_pattern = f'{Direction.SOUTH.value}|{Direction.NORTH.value}|'\
            f'{Direction.WEST.value}|{Direction.EAST.value}';

        self._raw_pattern = rf'^\s*PLACE\s+'\
            rf'({integer_pattern})+\s*,\s*({integer_pattern})\s*,\s*({direction_pattern})$'
        self._pattern = re.compile(self._raw_pattern)

    def _execute(self, command):
        match = self._pattern.match(command)
        x = int(match.group(1))
        y = int(match.group(2))
        face = Direction(match.group(3))
        Robot().place(x, y, face)



    def isAcceptable(self, command):
        return self._pattern.match(command)


class MoveCommand(Command):
    def help(self):
        return 'MOVE -> Robot move one step toward its direction.'

    def __init__(self):
        self._pattern = re.compile(r'^\s*MOVE\s*$')

    def _execute(self, command):
        Robot().move()

    def isAcceptable(self, command):
        return self._pattern.match(command)
        

class LeftCommand(Command):
    def help(self):
        return 'LEFT -> Force robot to turn left.'

    def __init__(self):
        self._pattern = re.compile(r'^\s*LEFT\s*$')

    def _execute(self, command):
        Robot().turn_left()

    def isAcceptable(self, command):
        return self._pattern.match(command)

class FindPathCommand(Command):
    def help(self):
        return 'FIND_PATH X,Y -> Print the path that robot should take to go to X and Y'\

    def __init__(self):
        integer_pattern = r'\d+'

        self._raw_pattern = rf'^\s*FIND_PATH\s+'\
            rf'({integer_pattern})+\s*,\s*({integer_pattern})\s*$'
        self._pattern = re.compile(self._raw_pattern)

    def _execute(self, command):
        match = self._pattern.match(command)
        x = int(match.group(1))
        y = int(match.group(2))
        return Robot().find_path(x,y)


    def isAcceptable(self, command):
        return self._pattern.match(command)