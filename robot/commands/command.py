from abc import ABC, abstractmethod

class Command(ABC):

    def execute(self, command: str) -> str:
        if not self.isAcceptable(command):
            raise InvalidInputException
        return self._execute(command)

    @abstractmethod
    def isAcceptable(self, command: str) -> bool:
        pass
    
    @abstractmethod
    def help(self):
        pass
    
    @abstractmethod
    def _execute(self, command: str) -> str:
        pass

class InvalidInputException(Exception):
    pass