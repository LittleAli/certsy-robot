
class Interpreter():
    def __init__(self):
        self.commands = set()
    
    def register(self, command):
        self.commands.add(command)

    def execute(self, input):
        for command in self.commands:
            if command.isAcceptable(input):
                return command.execute(input)
        raise NoCommandFoundException()

    def help(self):
        helps = []
        for command in self.commands:
            helps.append(command.help())
        return '.\n'.join(helps)

class NoCommandFoundException(Exception):
    pass
