# Development
Guidlines to extend and improve Certsy-robot.

## Add more command
You can simply add a new command by adding new command class to [robots_commands.py](../robot/commands/robot_commands.py). Added class must implement [command](../robot/commands/command.py) class abstract methods.
1. isAcceptable method checks if a command (string) is acceptable as input to this command.
```
    @abstractmethod
    def isAcceptable(self, command):
        pass
```
2. __execute method executes an input. You dont need to check acceptance of input, the parent class checks it using isAcceptable method before calling __execute. You can return the result of command in string type in result.
```
    @abstractmethod
    def _execute(self, command):
        pass
```

## Using robot module
You can use robot module in different application. You should import interpreter and register commands. You must initialized a table with positive dimentions. Also you must do appropriate error handling.
you could see one example of using this module in cli application in [cli.py](../cli.py)

### Exception and when they raised

1. IllegalMovementException:

When requested move, will change the location of robot outside of table.

2. NoCommandFoundException:

When there is no command that processes the input.

3. NotInitializedException:

Robot returns this exception unless you place the robot inside the table.

4. NegativeDimentionException

If you initilize table with negative dimentions, you get this exception.