# Certsy-robot

This project is a solution provided by Ali Nosrato to [Certsy robot coding test](docs/20_coding_test.md)

# How to run

## First way: using pipenv
You can run this project using pipenv.

1. Installation of pipenv:

```
pip install pipenv
```

2. Install dependencies:

```
pipenv install
```

3. Run cli:

```
pipenv run python cli.py
```

If you want to run tests, you should install dev dependencies. You just need to run second command with --dev flag. 

```
pipenv install --dev
```

Then you could run tests with this command:

```
pipenv run python -m pytest
```

## Second way: using Docker
You can run your command against docker using this command:

```
docker run -it littleali/certsy-robot-cli:latest
```

# Development
You can read [development readme](docs/development.md) to know more about the implementation.