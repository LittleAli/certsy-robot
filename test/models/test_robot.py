import unittest
from robot.models.robot import Robot, NotInitializedException, IllegalMovementException
from robot.models.directions import Direction
from robot.models.table import Table


class UninitializedRobotTest(unittest.TestCase):

    def test_if_not_initialized_robot_raise_error_on_turn_right(self):
        robot = Robot()
        with self.assertRaises(NotInitializedException):
            robot.turn_right()
    
    def test_if_not_initialized_robot_raise_error_on_turn_left(self):
        robot = Robot()
        with self.assertRaises(NotInitializedException):
            robot.turn_left()
    
    def test_if_not_initialized_robot_raise_error_on_report(self):
        robot = Robot()
        with self.assertRaises(NotInitializedException):
            robot.report()

    def test_if_not_initialized_robot_raise_error_on_move(self):
        robot = Robot()
        with self.assertRaises(NotInitializedException):
            robot.move()

class InitializedRobotTest(unittest.TestCase):

    def setUp(self):
        table = Table()
        table.initialize(4,4)
    
    def test_if_turn_left_works(self):
        robot = Robot()
        robot.place(2,2,Direction.NORTH)
        robot.turn_left()
        assert robot.get_face() == Direction.WEST
        robot.turn_left()
        assert robot.get_face() == Direction.SOUTH
        robot.turn_left()
        assert robot.get_face() == Direction.EAST
        robot.turn_left()
        assert robot.get_face() == Direction.NORTH

    def test_if_turn_right_works(self):
        robot = Robot()        
        robot.place(2,2,Direction.NORTH)
        robot.turn_right()
        assert robot.get_face() == Direction.EAST
        robot.turn_right()
        assert robot.get_face() == Direction.SOUTH
        robot.turn_right()
        assert robot.get_face() == Direction.WEST
        robot.turn_right()
        assert robot.get_face() == Direction.NORTH

    def test_if_move_works(self):
        robot = Robot()
        robot.place(2,2,Direction.NORTH)
        robot.move()
        assert robot.get_face() == Direction.NORTH
        assert robot.get_x() == 2
        assert robot.get_y() == 3
        with self.assertRaises(IllegalMovementException):
            robot.move()
    
class InitializedRobotTestWithObstacles(unittest.TestCase):
    
    def setUp(self):
        table = Table()
        table.initialize(5,5,[(1,3),(3,4)])

    def test_if_pacing_robot_on_an_obstacle_raise_exception(self):
        robot = Robot()
        with self.assertRaises(IllegalMovementException):
            robot.place(1, 3, Direction.NORTH)
    
    def test_if_pacing_robot_on_an_empty_cell(self):
        robot = Robot()
        robot.place(1, 4, Direction.NORTH)
        assert robot.get_face() == Direction.NORTH

    def test_find_path(self):
        robot = Robot()
        robot.place(1,1, Direction.NORTH)
        path = robot.find_path(1,2)
        assert path == [(1, 0), (0, 1), (1, 0), (0, 1), (1, 2)]

     
        

if __name__ == '__main__':
    unittest.main()
