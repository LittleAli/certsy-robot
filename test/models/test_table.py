import unittest
from robot.models.table import Table, NegativeDimentionException


class TableTest(unittest.TestCase):

    def test_if_table_includes_negative_positions(self):
        table = Table()
        table.initialize(3,4)
        assert not table.includes(1,-1)
        assert not table.includes(2,4)
        assert not table.includes(-3,2)
        assert not table.includes(3,2)

    def test_if_obstacles_occupy_the_cell(self):
        table = Table()
        table.initialize(5,5, [(2,3),(3,1)])
        assert not table.occupied_with_obstacles(1,2)
        assert not table.occupied_with_obstacles(3,2)
        assert table.occupied_with_obstacles(2,3)
        assert table.occupied_with_obstacles(3,1)

    def test_adjacent_cell_method(self):
        table = Table()
        table.initialize(5,5, [(2,3),(3,1)])
        adjacent = table.get_adjacent_cells(2,2)
        assert not (2,3) in adjacent
        assert (2,1) in adjacent

        adjacent = table.get_adjacent_cells(0,0)
        assert not (-1,0) in adjacent
        assert (1,0) in adjacent

    def test_if_negative_dimention_in_table_initialization_raise_exception(self):
        table = Table()
        with self.assertRaises(NegativeDimentionException): 
            table.initialize(1,-1)

if __name__ == '__main__':
    unittest.main()
