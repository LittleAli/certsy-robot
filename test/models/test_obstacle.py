import unittest
from robot.models.obstacle import Obstacle

class ObstacleInitiationTest(unittest.TestCase):

    def test_initiation(self):
        obstacle = Obstacle(2,3)
        assert obstacle.get_x() == 2
        assert obstacle.get_y() == 3
