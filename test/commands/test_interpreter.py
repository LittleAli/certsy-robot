import unittest
from robot.commands.interpreter import Interpreter, NoCommandFoundException
from robot.models.table import Table
from robot.models.robot import Robot
from robot.commands.robot_commands import RightCommand, LeftCommand, MoveCommand, PlaceCommand, ReportCommand



class InterpreterTest(unittest.TestCase):

    def setUp(self):
        self.interpreter = Interpreter()
        self.interpreter.register(RightCommand())
        self.interpreter.register(LeftCommand())
        self.interpreter.register(MoveCommand())
        self.interpreter.register(PlaceCommand())
        self.interpreter.register(ReportCommand())
        Table().initialize(5,5)
    
    def tearDown(self):
        Robot()._initialized = False

    def test_if_wrong_command_raise_exception(self):
        
        interpreter = Interpreter()
        interpreter.register(LeftCommand())
        with self.assertRaises(NoCommandFoundException):
            interpreter.execute("RIGHT")

    def test_if_robot_works_correctly_scenario_1(self):
       self.__test_from_file_helper("test/data/test-data-1.txt", "test/data/output-1.txt")

    def test_if_robot_works_correctly_scenario_1(self):
        self.__test_from_file_helper("test/data/test-data-2.txt", "test/data/output-2.txt")

    def __test_from_file_helper(self, input_filename, output_filename):
        input_lines = self.__file_reader_helper(input_filename)
        expected_output_lines = self.__file_reader_helper(output_filename)
        execution_output_lines = []
        for line in input_lines:
            if result := self.interpreter.execute(line):
                execution_output_lines.append(result)
        print('expected', expected_output_lines)
        print ('execution', execution_output_lines)
        assert expected_output_lines == execution_output_lines

    def __file_reader_helper(self, filename):
        with open(filename) as file:
            lines = file.readlines()
        return lines

if __name__ == '__main__':
    unittest.main()
    