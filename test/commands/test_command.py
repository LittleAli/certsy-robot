import unittest
from robot.commands.robot_commands import LeftCommand, RightCommand, MoveCommand, PlaceCommand, ReportCommand
from robot.commands.command import InvalidInputException

class CommandTest(unittest.TestCase):

    def test_if_right_command_accept_correct_input(self):
        rightCommand = RightCommand()
        assert rightCommand.isAcceptable("RIGHT")
    
    def test_if_left_command_accept_correct_input(self):
        leftCommand = LeftCommand()
        assert leftCommand.isAcceptable("LEFT")

    def test_if_move_command_accept_correct_input(self):
        moveCommand = MoveCommand()
        assert moveCommand.isAcceptable("MOVE")
        
    def test_if_report_command_accept_correct_input(self):
        reportCommand = ReportCommand()
        assert reportCommand.isAcceptable("REPORT")
    
    def test_if_place_command_accept_correct_input(self):
        placeCommand = PlaceCommand()
        assert placeCommand.isAcceptable("PLACE 2,3,NORTH")
        assert placeCommand.isAcceptable("PLACE 2,3,SOUTH")
        assert placeCommand.isAcceptable("PLACE 2,3,EAST")
        assert placeCommand.isAcceptable("PLACE 2,3,WEST")
    
    def test_if_right_command_reject_incorrect_input(self):
        rightCommand = RightCommand()
        assert not rightCommand.isAcceptable("RiGHT")
    
    def test_if_left_command_reject_incorrect_input(self):
        leftCommand = LeftCommand()
        assert not leftCommand.isAcceptable("LEFTa")

    def test_if_move_command_reject_incorrect_input(self):
        moveCommand = MoveCommand()
        assert not moveCommand.isAcceptable("sMOVE")
        
    def test_if_report_command_reject_incorrect_input(self):
        reportCommand = ReportCommand()
        assert not reportCommand.isAcceptable("REPORt")
    
    def test_if_place_command_reject_incorrect_input(self):
        placeCommand = PlaceCommand()
        assert not placeCommand.isAcceptable("PLACE 2,3NORTH")
        assert not placeCommand.isAcceptable("PLACE 2,3,dSOUTH")
        assert not placeCommand.isAcceptable("PLACE -2,3,EAST")
        assert not placeCommand.isAcceptable("PLACE2,33,WEST")
    
    def test_if_right_command_execution_of_incorrect_input_raise_exception(self):
        rightCommand = RightCommand()
        with self.assertRaises(InvalidInputException):
            rightCommand.execute("RiGHT")
    
    def test_if_left_command_execution_of_incorrect_input_raise_exception(self):
        leftCommand = LeftCommand()
        with self.assertRaises(InvalidInputException):
            leftCommand.execute("left0")

    def test_if_move_command_execution_of_incorrect_input_raise_exception(self):
        moveCommand = MoveCommand()
        with self.assertRaises(InvalidInputException):
            moveCommand.execute("Move")
        
    def test_if_report_command_execution_of_incorrect_input_raise_exception(self):
        reportCommand = ReportCommand()
        with self.assertRaises(InvalidInputException):
            reportCommand.execute("REPORt")

    def test_if_place_command_execution_of_incorrect_input_raise_exception(self):
        placeCommand = PlaceCommand()
        with self.assertRaises(InvalidInputException):
            placeCommand.execute("PLACE2,3,SOUTH")

if __name__ == '__main__':
    unittest.main()
    