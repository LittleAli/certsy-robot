from robot.commands.interpreter import Interpreter, NoCommandFoundException
from robot.commands.robot_commands import RightCommand, LeftCommand, MoveCommand, PlaceCommand, ReportCommand, FindPathCommand
from robot.models.table import Table, NegativeDimentionException
from robot.models.robot import NotInitializedException, IllegalMovementException, Robot
import configparser
import json

import fileinput

def parse_tuple(obstacles_string):
    obstacles=[]
    for location in obstacles_string.split('|'):
        (x,y) = tuple(int(k) for k in location.split(','))
        obstacles.append((x,y))
    return obstacles

if __name__ == '__main__':
    #Reading configuration
    config = configparser.ConfigParser()
    config.read('setting.ini')
    table_width = int(config.get('table', 'width'))
    table_height = int(config.get('table', 'height'))
    obstacles_string = config.get('table', 'obstacles')
    obstacles = parse_tuple(obstacles_string)
    
    run_in_verbose_mod = config.get('cli', 'verbose') == 'True'

    #Setting up everey thing
    interpreter = Interpreter()
    interpreter.register(RightCommand())
    interpreter.register(LeftCommand())
    interpreter.register(MoveCommand())
    interpreter.register(PlaceCommand())
    interpreter.register(ReportCommand())
    interpreter.register(FindPathCommand())

    table = Table()
    try:
        table.initialize(table_width,table_height, obstacles)
    except NegativeDimentionException:
        print(f'You cannot initiate table with negative dimentions. Plase check you config file.')

    for line in fileinput.input():
        try:
            if run_in_verbose_mod:
                print("State before execution: ", Robot().report())
            if response := interpreter.execute(line):
                print(response)
            if run_in_verbose_mod:
                print("State after execution: ", Robot().report())

        except IllegalMovementException:
            if run_in_verbose_mod:
                print('I cannot do your command. I will drop of the table.')

        except NoCommandFoundException:
            if run_in_verbose_mod:
                print(interpreter.help())

        except NotInitializedException:
            if run_in_verbose_mod:
                print("You should first, place your robot on the table using PLACE command.")
                print(interpreter.help())
